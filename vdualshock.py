import threading
import time
import logging

import mouse
import pyautogui
import pynput
import vgamepad as vg
import virtualkey
import subprocess

# Reccomended: increment the DPI value of your mouse when using this program
# Download accelSwitch.exe from https://github.com/jan-glx/accelSwitch and put it in PATH to automatically toggle mouse acceleration when needed

smoothing = 1  # int only, useful with a high pooling rate mouse, 0 disables it
sensibility = 3  # float, 0 < sens < 10
default_mouse_accel = 'on'  # 'on' or 'off'


def normalize(x):
    x *= RATIO
    result = x / (1 + x ** 2) ** 0.5
    # result = max(min(x, 1), -1)
    # print(result)
    return result


class TimedEvent(threading.Event):
    def __init__(self):
        super().__init__()
        self.timestamp = None

    def set(self):
        super().set()
        self.timestamp = time.time()

    def wait(self, timeout=None):
        super().wait(timeout=timeout)
        return self.timestamp

    def clear(self):
        super().clear()
        self.timestamp = None


class MouseMovements:
    def __init__(self):
        self.movements = [0, 0]
        self.times = 0
        self.last_xy = (0, 0)

    def adds(self, x, y):
        self.movements[0] += x - self.last_xy[0]
        self.movements[1] += self.last_xy[1] - y
        self.times += 1
        self.last_xy = (x, y)

    def retrieve(self):
        self.times = 0
        m = tuple(self.movements)
        self.movements = [0, 0]
        return m


class IdleLoop(threading.Thread):
    def __init__(self):
        super().__init__(daemon=True)

    def run(self) -> None:
        while True:
            ts = event.wait()
            time.sleep(.005)
            if event.timestamp == ts:
                gamepad.right_joystick_float(0, 0)
                gamepad.update()
                movements.retrieve()


class Bindings:
    def __init__(self, name: str, keyboard_map, mouse_map, trigger: int = 0, hook=True):
        if not name:
            raise ValueError('name cannot be empty')
        self.name = name
        self.trigger = trigger
        self.hook = hook
        self.keyboard_map = keyboard_map
        self.leftstick_map = {i: value for (i, value) in (keyboard_map | mouse_map).items() if isinstance(value, list)}
        self.dpad_directions = get_dpad_directions({i: value for (i, value) in (keyboard_map | mouse_map).items() if isinstance(value, vg.DS4_DPAD_DIRECTIONS)})
        self.mouse_map = mouse_map
        self.press_kbs = {i: associate(value, press=1, button=i) for (i, value) in (keyboard_map | mouse_map).items()}
        self.release_kbs = {i: associate(value, press=0, button=i) for (i, value) in (keyboard_map | mouse_map).items()}


class BindingsHandler:
    def __init__(self):
        self.bindings = {'default': Bindings('default', {}, {}, hook=False)}
        self._triggers = {0: lambda: self.switch('default')}
        self.hooked = False

        self.dpad_keys_state = set()
        self.leftstick_keys_state = set()

        self.name = 'default'
        self.keyboard_map = {} | self._triggers
        self.mouse_map = {}
        self.dpad_directions = {}
        self.leftstick_map = {}
        self.press_kbs = {} | self._triggers
        self.release_kbs = {}

    def switch(self, name):
        logger.info(f'Switching from {self.name} to {name}')
        if name not in self.bindings:
            raise KeyError(f'No bindings named {name}')
        if self.name == 'default':
            self.change_trigger(self.bindings[name].trigger, 'default')
        else:
            self.change_trigger(self.bindings[self.name].trigger, self.name)
            self.change_trigger(self.bindings[name].trigger, 'default')

        self.press_kbs = self.bindings[name].press_kbs | self._triggers
        self.release_kbs = self.bindings[name].release_kbs
        self.keyboard_map = self.bindings[name].keyboard_map | self._triggers
        self.mouse_map = self.bindings[name].mouse_map
        self.dpad_directions = self.bindings[name].dpad_directions
        self.leftstick_map = self.bindings[name].leftstick_map
        if self.hooked != self.bindings[name].hook:
            self.hooked = self.bindings[name].hook
            try:
                subprocess.check_output(['accelSwitch.exe', {True: 'off', False: default_mouse_accel}[self.hooked]])
            except FileNotFoundError:
                logger.critical('Download accelSwitch.exe from https://github.com/jan-glx/accelSwitch and put it in your PATH')
        self.name = name
        logger.info('Switch completed')

    def add_bindings(self, binding):
        self.bindings[binding.name] = binding
        if binding.trigger not in self._triggers:
            self.change_trigger(binding.trigger, binding.name)
        else:
            raise ValueError('Trigger already assigned')

    def change_trigger(self, vk, name):
        self._triggers[vk] = lambda: self.switch(name)
        if self.name:
            self.press_kbs = self.bindings[name].press_kbs | self._triggers
            self.keyboard_map = self.bindings[name].keyboard_map | self._triggers

    def dpad_keys_state_add(self, vk):
        self.dpad_keys_state.add(vk)

    def dpad_keys_state_remove(self, vk):
        self.dpad_keys_state.remove(vk)

    def dpad_state(self):
        return self.dpad_directions.get(frozenset(self.dpad_keys_state), vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NONE)

    def leftstick_keys_state_add(self, vk):
        self.leftstick_keys_state.add(vk)

    def leftstick_keys_state_remove(self, vk):
        self.leftstick_keys_state.remove(vk)

    def leftstick_state(self):
        stick = [0, 0]
        for vk in self.leftstick_keys_state:
            stick[self.leftstick_map[vk][0]] += self.leftstick_map[vk][1]
        return stick


def get_dpad_directions(dpad_map):
    reversed_dpad_map = {value: key for (key, value) in dpad_map.items()}
    dpad_directions = {frozenset([i]): dpad_map[i] for i in dpad_map}
    if vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTH in dpad_map.values() and vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST in dpad_map.values():
        dpad_directions[frozenset({reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTH], reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST]})] = vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTHWEST
    if vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST in dpad_map.values() and vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTH in dpad_map.values():
        dpad_directions[frozenset({reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST], reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTH]})] = vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTHWEST
    if vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTH in dpad_map.values() and vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST in dpad_map.values():
        dpad_directions[frozenset({reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTH], reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST]})] = vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTHEAST
    if vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST in dpad_map.values() and vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTH in dpad_map.values():
        dpad_directions[frozenset({reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST], reversed_dpad_map[vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTH]})] = vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTHEAST
    return dpad_directions


def associate(action, press, button=None):
    if isinstance(action, vg.DS4_BUTTONS):
        if action == vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_LEFT:
            if press:
                return lambda: [gamepad.press_button(vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_LEFT), gamepad.left_trigger_float(1)]
            else:
                return lambda: [gamepad.release_button(vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_LEFT), gamepad.left_trigger_float(0)]
        elif action == vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_RIGHT:
            if press:
                return lambda: [gamepad.press_button(vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_RIGHT), gamepad.right_trigger_float(1)]
            else:
                return lambda: [gamepad.release_button(vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_RIGHT), gamepad.right_trigger_float(0)]
        else:
            if press:
                return lambda: gamepad.press_button(action)
            else:
                return lambda: gamepad.release_button(action)
    elif isinstance(action, vg.DS4_SPECIAL_BUTTONS):
        if press:
            return lambda: gamepad.press_special_button(action)
        else:
            return lambda: gamepad.release_special_button(action)
    elif isinstance(action, list):
        if press:
            return lambda: [bindings.leftstick_keys_state_add(button), gamepad.left_joystick_float(*bindings.leftstick_state())]
        else:
            return lambda: [bindings.leftstick_keys_state_remove(button), gamepad.left_joystick_float(*bindings.leftstick_state())]
    elif isinstance(action, vg.DS4_DPAD_DIRECTIONS):
        if press:
            return lambda: [bindings.dpad_keys_state_add(button), gamepad.directional_pad(bindings.dpad_state())]
        else:
            return lambda: [bindings.dpad_keys_state_remove(button), gamepad.directional_pad(bindings.dpad_state())]
    else:
        return action


def join():
    listener.join()


def on_press(k):
    if isinstance(k, pynput.keyboard.Key):
        vk = k.value.vk
    elif isinstance(k, pynput.keyboard.KeyCode):
        vk = k.vk
    else:
        raise AttributeError(f'vk not found in {k}')
    if vk in bindings.keyboard_map:
        bindings.press_kbs[vk]()
    else:
        print(f'{virtualkey.vk_key(vk)} - {vk}')
    gamepad.update()


def on_release(k):
    if isinstance(k, pynput.keyboard.Key):
        vk = k.value.vk
    elif isinstance(k, pynput.keyboard.KeyCode):
        vk = k.vk
    else:
        raise AttributeError(f'vk not found in {k}')
    if vk in bindings.release_kbs:
        bindings.release_kbs[vk]()
    gamepad.update()


def win32_event_filter(_, data):
    if data.vkCode in bindings.keyboard_map:
        listener._suppress = True
    else:
        listener._suppress = False
        return True


def on_move(ev):
    if isinstance(ev, mouse.MoveEvent) and bindings.hooked:
        event.clear()
        x = ev.x
        y = ev.y
        movements.adds(x, y)
        if movements.times > smoothing:
            m = movements.retrieve()
            gamepad.right_joystick_float(normalize(m[0]), normalize(m[1]))
            if not (-DISTANCE_FROM_HOME < x-HOME_X < DISTANCE_FROM_HOME and -DISTANCE_FROM_HOME < y-HOME_Y < DISTANCE_FROM_HOME):
                movements.last_xy = (HOME_X, HOME_Y)
                pyautogui.moveTo(HOME_X, HOME_Y)
        event.set()
    elif isinstance(ev, mouse.ButtonEvent):
        if ev.button in bindings.mouse_map:
            if ev.event_type in ('down', 'double'):
                bindings.press_kbs[ev.button]()
            elif ev.event_type == 'up':
                bindings.release_kbs[ev.button]()
    elif isinstance(ev, mouse.WheelEvent):
        if ev.delta in bindings.mouse_map:
            bindings.press_kbs[ev.delta]()
            gamepad.update()
            bindings.release_kbs[ev.delta]()
    gamepad.update()


logging.basicConfig(level=logging.INFO)
pyautogui.PAUSE = 0
pyautogui.FAILSAFE = False
RATIO = sensibility * .1 / (smoothing + 1)
HOME_X = pyautogui.size()[0] // 2
HOME_Y = pyautogui.size()[1] // 2
DISTANCE_FROM_HOME = 300
logger = logging.getLogger(__name__)
event = TimedEvent()
movements = MouseMovements()
bindings = BindingsHandler()
gamepad = vg.VDS4Gamepad()

listener = pynput.keyboard.Listener(on_press=on_press, on_release=on_release, win32_event_filter=win32_event_filter)


def start():
    listener.__enter__()
    mouse.hook(on_move)
    IdleLoop().start()
