import virtualkey
import vgamepad as vg
import vdualshock


keybindings = {
    virtualkey.VK_X: vg.DS4_BUTTONS.DS4_BUTTON_SQUARE,
    virtualkey.VK_2: vg.DS4_BUTTONS.DS4_BUTTON_SQUARE,
    virtualkey.VK_SPACE: vg.DS4_BUTTONS.DS4_BUTTON_CROSS,
    virtualkey.VK_LCONTROL: vg.DS4_BUTTONS.DS4_BUTTON_CIRCLE,
    virtualkey.VK_F: vg.DS4_BUTTONS.DS4_BUTTON_TRIANGLE,
    virtualkey.VK_1: vg.DS4_BUTTONS.DS4_BUTTON_TRIANGLE,
    virtualkey.VK_Q: vg.DS4_BUTTONS.DS4_BUTTON_SHOULDER_LEFT,
    virtualkey.VK_E: vg.DS4_BUTTONS.DS4_BUTTON_SHOULDER_RIGHT,
    virtualkey.VK_C: vg.DS4_BUTTONS.DS4_BUTTON_SHOULDER_RIGHT,
    virtualkey.VK_OEM_5: vg.DS4_BUTTONS.DS4_BUTTON_SHARE,
    virtualkey.VK_OEM_6: vg.DS4_BUTTONS.DS4_BUTTON_OPTIONS,
    virtualkey.VK_ESCAPE: vg.DS4_BUTTONS.DS4_BUTTON_OPTIONS,
    virtualkey.VK_LSHIFT: vg.DS4_BUTTONS.DS4_BUTTON_SHOULDER_LEFT,
    virtualkey.VK_Z: vg.DS4_BUTTONS.DS4_BUTTON_THUMB_LEFT,
    virtualkey.VK_V: vg.DS4_BUTTONS.DS4_BUTTON_THUMB_RIGHT,

    virtualkey.VK_END: vg.DS4_SPECIAL_BUTTONS.DS4_SPECIAL_BUTTON_TOUCHPAD,
    virtualkey.VK_HOME: vg.DS4_SPECIAL_BUTTONS.DS4_SPECIAL_BUTTON_PS,

    virtualkey.VK_UP: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_NORTH,
    virtualkey.VK_LEFT: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST,
    virtualkey.VK_DOWN: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_SOUTH,
    virtualkey.VK_RIGHT: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST,

    virtualkey.VK_D: [0, +1],
    virtualkey.VK_A: [0, -1],
    virtualkey.VK_W: [1, +1],
    virtualkey.VK_S: [1, -1],
}

mouse_bindings = {
    # 'x':,  # indietro
    'x2': vg.DS4_BUTTONS.DS4_BUTTON_SQUARE,  # avanti
    'left': vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_RIGHT,
    'right': vg.DS4_BUTTONS.DS4_BUTTON_TRIGGER_LEFT,
    # 'middle': ,
    1.0: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_EAST,  # wheel up
    -1.0: vg.DS4_DPAD_DIRECTIONS.DS4_BUTTON_DPAD_WEST,  # wheel down
}

vdualshock.bindings.add_bindings(vdualshock.Bindings('PS4', keybindings, mouse_bindings, trigger=virtualkey.VK_RSHIFT))
vdualshock.bindings.switch('PS4')

vdualshock.start()
vdualshock.join()
